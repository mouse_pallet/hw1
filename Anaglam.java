import java.io.*;
import java.util.HashMap;
import java.util.Random;


class Anaglam{

   static HashMap<Character,Integer> dicmap = new HashMap<Character,Integer>();//アルファベット⇔素数
    static HashMap<Float,String> primemap = new HashMap<Float,String>();//素数乗算⇔英単語
    public static void main (String[ ] args) throws IOException {
    	;
     
     Dictionary();//辞書mapの作成

     //辞書→素数変換
     //ファイルを読み込む
     try{
         File file = new File("/usr/share/dict/american-english");

         if (checkBeforeReadfile(file)){
           BufferedReader br = new BufferedReader(new FileReader(file));

           String str;
           while((str = br.readLine()) != null){
             //System.out.println(str);
             primemap.put(Prime(str),str);
           }

           br.close();
         }else{
           System.out.println("ファイルが見つからないか開けません");
         }
       }catch(FileNotFoundException e){
         System.out.println(e);
       }catch(IOException e){
         System.out.println(e);
       }

     
     
     //文字入力
     System.out.println("アルファベット列を入力してください。");
     BufferedReader input =
         new BufferedReader (new InputStreamReader (System.in));
    String str = input.readLine( );
    float multana=Prime(str);//入力した単語を素数変換
    Match(multana);  
}

    
    private static boolean checkBeforeReadfile(File file){
        if (file.exists()){
          if (file.isFile() && file.canRead()){
            return true;
          }
        }

        return false;
      }

    //単語→素数
   static public float Prime(String word){
       String loword=word.toLowerCase();
	char  array[]  = loword.toCharArray();
	float mult=1;

	for(int i=0;i<array.length;i++){
	    if(dicmap.get(array[i])!=null){
	       mult=mult*dicmap.get(array[i]);
		}
	}
	return mult;
   }

   //辞書mapの作成
    static public void Dictionary(){
    	dicmap.put('e', 2);
    	dicmap.put('a', 3);
    	dicmap.put('t', 5);
    	dicmap.put('i', 7);
    	dicmap.put('o', 11);
	dicmap.put('s', 13);
        dicmap.put('n', 17);
        dicmap.put('r', 19);
        dicmap.put('h', 23);
        dicmap.put('l', 29);
        dicmap.put('d', 31);
        dicmap.put('c', 37);
        dicmap.put('u', 41);
        dicmap.put('m', 43);
        dicmap.put('p', 47);
        dicmap.put('f', 53);
        dicmap.put('g', 59);
        dicmap.put('y', 61);
        dicmap.put('w', 67);
        dicmap.put('b', 71);
        dicmap.put('v', 73);
        dicmap.put('k', 79);
        dicmap.put('j', 83);
        dicmap.put('x', 89);
        dicmap.put('q', 97);
        dicmap.put('z', 101);
        dicmap.put('\'',103);//'を文字として扱う

    }
   static void Match (Float mana){
    	char c;
    	int j=1;
    	if(primemap.containsKey(mana)){//計算した数値はmapに入っているか
    		System.out.println(primemap.get(mana));//mapから単語出力
    		return;
    	}
    	else{
    		while(j<=26){
    			c=(char) Math.floor( Math.random() * (j) + 97 );
    			if(mana%dicmap.get(c)==0&&mana!=1){
    				Match(mana/dicmap.get(c));
    				break;
    			}
    			else{ if(mana!=1){j++;}
    				else{System.out.println("no anaglam");}
    			}
    			
    		}
    	}
   }
    
}

Yuko Yanagawa
Yuko Yanagawa
